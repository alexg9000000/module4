﻿using System;
using System.Linq;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!!!");
        }
        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                return array.Max();
            }
        }
        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                return array.Min();
            }
        }
        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                return array.Sum();
            }
        }
        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                return array.Max() - array.Min();
            }
        }
        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                int max = array.Max();
                int min = array.Min();
                for (int i = 0; i < array.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        array[i] += max;
                    }
                    else
                    {
                        array[i] -= min;
                    }
                }
            }
        }
        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }
        public int Task_2(int a, int b)
        {
            return a + b;
        }
        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }
        public string Task_2(string a, string b)
        {
            return a + b;
        }
        public int[] Task_2(int[] a, int[] b)
        {
            int[] array;
            if (a.Length > b.Length)
            {
                array = new int[a.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = (i < b.Length) ? (a[i] + b[i]) : a[i];
                    Console.Write("{0}, ", array[i]);
                }
                return array;
            }
            else if (a.Length < b.Length)
            {
                array = new int[b.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = (i < a.Length) ? (a[i] + b[i]) : b[i];
                    Console.Write("{0}, ", array[i]);
                }
                return array;
            }
            else
            {
                array = new int[b.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = a[i] + b[i];
                    Console.Write("{0}, ", array[i]);
                }
                return array;
            }
        }
        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }
        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentException("radius");
            }
            else
            {
                length = 2 * Math.PI * radius;
                square = Math.PI * radius * radius;
            }
        }
        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = array.Max();
            minItem = array.Min();
            sumOfItems = array.Sum();
        }
        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            int a = numbers.Item1 + 10;
            int b = numbers.Item2 + 10;
            int c = numbers.Item3 + 10;
            return (a, b, c);
        }
        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException("radius");
            }
            else
            {
                double length = 2 * Math.PI * radius;
                double square = Math.PI * radius * radius;
                return (length, square);
            }
        }
        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                return (array.Min(), array.Max(), array.Sum());
            }
        }
        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] += 5;
                }
            }
        }
        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array");
            }
            else
            {
                if (direction == SortDirection.Ascending)
                {
                    Array.Sort(array);
                }
                else
                {
                    Array.Sort(array);
                    Array.Reverse(array);
                }
            }
        }
        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }
            double ya, yb;
            result = (x1 + x2) / 2;
            ya = func(x1);
            yb = func(result);
            if (x2 - x1 > e && yb != 0)
            {
                if (ya * yb < 0)
                {
                    x2 = result;
                    Task_7(func, x1, x2, e, result);
                }
                else
                {
                    x1 = result;
                    Task_7(func, x1, x2, e, result);
                }
            }
            return result;
        }
    }
}
